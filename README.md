# zbstudio-plugins collection

This is a collection of partly self-made plugins and plugins from the official 
ZeroBrane Repository which you can find here: https://github.com/pkulchenko/ZeroBranePackage.

1. Download the plugin files on put them in the package directory inside your zbstudio installation
2. Start zbstudio and click on Edit -> Preferences -> settings:user
3. You can configure each plugin individually by adding one/all of these line

```lua
-- Highlighting function calls, you set the highlighting color here
highlightfunctioncalls = {indicator = wxstc.wxSTC_INDIC_TEXTFORE, color = styles.colors.Blue}
-- Highlighting tables key values: mytable.key1 would highlight key1 in a color
highlightproperties = {indicator = wxstc.wxSTC_INDIC_TEXTFORE, color = styles.colors.Red}
```